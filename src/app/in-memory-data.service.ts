import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable()
export class InMemoryDataService implements InMemoryDbService {

  createDb(){
    let missions = [
      { id: 1, name: 'one', date: 'date', notes: 'Red Dragon is a planned '},
      { id: 2, name: 'two', date: 'date', notes: 'Red Dragon is a planned '}
    ];
    return { missions };
  }
}
