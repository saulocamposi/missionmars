import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Mission } from './mission';

@Injectable()
export class MissionService {

  private missionsUrl = 'api/missions'; //URL to web api
  private headers = new Headers({'Content-Type':'application/json'});

  constructor(private http: Http){ }

  getMissions(): Promise<Mission[]> {
  return this.http.get(this.missionsUrl)
    .toPromise()
    .then(response => response.json().data)
    .catch(this.handleError);
  }

  getHero(id:number): Promise<Mission>{
    const url = `${this.missionsUrl}/${id}`;

    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Mission)
      .catch(this.handleError);
  }

  update(mission : Mission): Promise<Mission> {
    const url = `${this.missionsUrl}/${mission.id}`;
    return this.http
      .put(url, JSON.stringify(mission),{ headers: this.headers })
      .toPromise()
      .then(() => mission)
      .catch(this.handleError);
  }

  create(name: String, date:String, notes:String): Promise<Mission>{
    return this.http
      .post(this.missionsUrl, JSON.stringify({name: name, date: date, notes: notes}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Mission)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.missionsUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

private handleError(error: any): Promise<any>{
    console.log('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
