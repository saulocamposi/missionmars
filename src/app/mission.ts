export class Mission {

  id: number;
  name: String;
  date: String;
  notes: String;
  status:String;
  description: String;
  
}
