import { Component, OnInit, Input } from '@angular/core';
import { Mission } from '../mission';
import { MissionService } from '../mission.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-mission',
  templateUrl: './mission.component.html',
  styleUrls: ['./mission.component.css'],
  providers: [ MissionService ]
})
export class MissionComponent implements OnInit {

  constructor(private router: Router,private missionService: MissionService) { }
  title = 'Mission';
  @Input() missions: Mission[];
  mission: Mission;

  selectedMission: Mission;


  onSelect(mission: Mission): void{
    this.selectedMission = mission;
  }

  getMissions():void{
    this.missionService.getMissions().then(missions => this.missions = missions);
  }

  ngOnInit():void{
    this.getMissions();
  }

  add(name: string, launchDate: string,notes: string): void {
    name = name.trim();
    if(!name){return;}
    this.missionService.create(name, launchDate, notes)
      .then(mission => {
        this.missions.push(mission);
        this.selectedMission = null;
      });
  }


}
