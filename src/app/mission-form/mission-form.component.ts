
import { Component, OnInit, Input } from '@angular/core';
import { Mission } from '../mission';
import { MissionService } from '../mission.service';

@Component({
  selector: 'app-mission-form',
  templateUrl: './mission-form.component.html',
  styleUrls: ['./mission-form.component.css'],
  providers: [ MissionService ]
})
export class MissionFormComponent implements OnInit {

mission: Mission;
missions: Mission[];
  constructor(private missionService: MissionService) { }

  getMissions():void{
    this.missionService.getMissions().then(missions => this.missions = missions);
  }

  ngOnInit():void{
    this.getMissions();
  }

  add(mission: Mission): void {
    console.log(mission.name);
    console.log(mission.date);
    mission.name = mission.name.trim();
    if(!mission.name){return;}
    this.missionService.create(mission.name, mission.date, mission.notes)
      .then(mission => {
        this.missions.push(mission);
        console.log("push");
        //this.selectedMission = null;
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any>{
      console.log('An error occurred', error);
      return Promise.reject(error.message || error);
    }


}
