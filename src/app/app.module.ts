import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { MissionDetailComponent } from './mission-detail/mission-detail.component';
import { MissionComponent } from './mission/mission.component';
import { MissionService } from './mission.service';
import { MissionFormComponent } from './mission-form/mission-form.component';


const appRoutes: Routes = [{  path: 'missions',  component: MissionComponent }];

@NgModule({
  declarations: [
    AppComponent,
    MissionDetailComponent,
    MissionComponent,
    MissionFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [MissionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
