import { Component, OnInit, Input } from '@angular/core';
import { Mission } from '../mission';
import { MissionService } from '../mission.service';


@Component({
  selector: 'app-mission-detail',
  templateUrl: './mission-detail.component.html',
  styleUrls: ['./mission-detail.component.css']
})
export class MissionDetailComponent implements OnInit {

  @Input() mission: Mission;
  missions: Mission[];
  selectedMission: Mission;

  constructor(private missionService: MissionService) { }

  getMissions():void{
    this.missionService.getMissions().then(missions => this.missions = missions);
  }

  ngOnInit():void{
    //this.getMissions();
  }

  delete(mission: Mission): void {
    this.missionService
        .delete(mission.id)
        .then(() => {
          console.log("it is deleting");
        });
  }

  save(): void {
      this.missionService.update(this.mission)
        .then(() => {
          console.log("it is updating");
        });
    }





}
