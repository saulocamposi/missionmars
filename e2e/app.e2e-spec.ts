import { MissionmarsPage } from './app.po';

describe('missionmars App', () => {
  let page: MissionmarsPage;

  beforeEach(() => {
    page = new MissionmarsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
